import { Form } from '@app/app/components';
import styles from './home.module.css';

export default function Home() {
  return (
    <main>
      <h1 className={styles.title}>AD Trade Form</h1>
      <Form />
    </main>
  );
}
