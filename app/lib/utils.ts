import { IGroup, ISubGroup } from '@app/interfaces';

export function subGroupFinderById(
  state,
  groupId,
  subGroupId,
): ISubGroup | null {
  const group: IGroup = state.groups.find((gr) => gr.id === groupId);
  const subGroup: ISubGroup | undefined =
    group &&
    group.subGroups &&
    group.subGroups.find((sbGr) => sbGr.id === subGroupId);

  if (subGroup && Object.keys(subGroup).length !== 0) {
    return subGroup;
  }

  return null;
}
