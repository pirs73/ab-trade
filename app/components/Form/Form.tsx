'use client';
import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import clsx from 'clsx';
import { v4 as uuidv4 } from 'uuid';
import { IForm, IGroup, ISubGroup } from '@app/interfaces';
import {
  changeEditableForm,
  addSubGroupForm,
  deleteSubGroupForm,
  addProductForm,
  deleteProductById,
  changeProductNameForm,
  changeProductPriceForm,
  changeProductCountForm,
  calculateSumForm,
  emptyEditableForm,
} from '@app/store/editableFormSlice';
import { Button } from '@app/app/components';
import { subGroupFinderById } from '@app/app/lib/utils';
import { FormProps } from './Form.props';
import styles from './form.module.css';

type T = string | number;

const Form = ({ ...props }: FormProps) => {
  const dispatch = useDispatch();
  const [nameError, setNameError] = useState<T[]>([]);
  const [priceError, setPriceError] = useState<T[]>([]);
  const [countError, setCountError] = useState<T[]>([]);
  const [stateForm, setStateForm] = useState<IForm | Record<string, any>>({});

  const { editableForm }: { editableForm: IForm } = useSelector(
    (state: { editableForm: IForm }) => ({
      ...state,
    }),
  );

  // console.log(editableForm);

  useEffect(() => {
    setStateForm(editableForm);
  }, [editableForm]);

  const onSubmit = async (e) => {
    e.preventDefault();
    console.info(JSON.stringify(stateForm));
    await dispatch(emptyEditableForm({}));
  };

  /** Добавление группы */
  const addGroup = async (e) => {
    e.preventDefault();
    const prevGroups: IGroup[] = [...editableForm.groups];
    prevGroups.push({
      id: uuidv4(),
      sum: 0,
      subGroups: [
        {
          id: uuidv4(),
          sum: 0,
          products: [
            {
              id: uuidv4(),
              name: `Product 1`,
              sum: 0,
              count: 0,
              price: 0,
            },
          ],
        },
      ],
    });

    const newState: IForm = { sum: editableForm.sum, groups: prevGroups };
    await dispatch(changeEditableForm({ ...newState }));
  };

  /** Удаление группы */
  const deleteGroup = async (e, groupId) => {
    e.preventDefault();

    const prevGroups: IGroup[] = [...editableForm.groups];
    const newGroups = prevGroups.filter((gr) => gr.id !== groupId);
    const newState: IForm = { sum: editableForm.sum, groups: newGroups };
    // setStateForm(newState);
    await dispatch(changeEditableForm({ ...newState }));
    await dispatch(calculateSumForm({ groupId }));
  };

  /** добавление субгруппы */
  const addSubGroup = async (e, groupId) => {
    e.preventDefault();
    await dispatch(addSubGroupForm({ id: groupId }));
  };

  /** Удаление субгруппы */
  const deleteSubGroup = async (e, groupId, subGroupId) => {
    e.preventDefault();

    await dispatch(deleteSubGroupForm({ groupId, subGroupId }));
    await dispatch(calculateSumForm({ groupId, subGroupId }));
  };

  /** Добавление продукта */
  const addProduct = async (e, groupId, subGroupId) => {
    e.preventDefault();
    await dispatch(addProductForm({ groupId, subGroupId }));
  };

  /** Удаление продукта */
  const deleteProduct = async (e, groupId, subGroupId, productId) => {
    e.preventDefault();
    await dispatch(deleteProductById({ groupId, subGroupId, productId }));
    await dispatch(calculateSumForm({ groupId, subGroupId }));
  };

  const handleSetName = async (
    groupId: string | number,
    subGroupId: string | number,
    id: string | number,
    e,
  ) => {
    const value: string = e.target.value;
    const subGroup = subGroupFinderById(editableForm, groupId, subGroupId);
    if (subGroup && subGroup.products && subGroup.products.length > 0) {
      const names = subGroup.products.map((pr) => {
        return pr.name;
      });

      if (!names.includes(value)) {
        setNameError((prev) => prev.filter((er) => er !== id));
        await dispatch(
          changeProductNameForm({
            groupId,
            subGroupId,
            id,
            name: value,
          }),
        );
      } else {
        nameError.push(id);
        setNameError(nameError);
      }
    }
  };

  const handleSetPrice = async (groupId, subGroupId, id, e) => {
    const value = e.target.value;
    const numValue = Number.parseFloat(value);
    const isAN = (value) => {
      return !Number.isNaN(value);
    };

    if (isAN(numValue)) {
      setPriceError((prev) => prev.filter((er) => er !== id));
      await dispatch(
        changeProductPriceForm({
          groupId,
          subGroupId,
          id,
          price: Number(numValue.toFixed(2)),
        }),
      );
      /** TODO: рассчитать изменение сумм */
      await dispatch(
        calculateSumForm({
          groupId,
          subGroupId,
          id,
          price: Number(numValue.toFixed(2)),
        }),
      );
    } else {
      priceError.push(id);
      setPriceError(priceError);
    }
  };

  const handleSetCount = async (groupId, subGroupId, id, e) => {
    const value = e.target.value;
    const numValue = Number.parseInt(value);
    const isAN = (value) => {
      return !Number.isNaN(value);
    };

    if (isAN(numValue)) {
      setCountError((prev) => prev.filter((er) => er !== id));
      await dispatch(
        changeProductCountForm({
          groupId,
          subGroupId,
          id,
          count: numValue,
        }),
      );
      /** TODO: рассчитать изменение сумм */
      await dispatch(
        calculateSumForm({
          groupId,
          subGroupId,
          id,
          count: numValue,
        }),
      );
    } else {
      countError.push(id);
      setCountError(countError);
    }
  };

  /** TODO: потом..., маска не подойдет, нужно создать массив кодов нужных клавиш */
  // function handleOnlyNumber(e) {
  //   const key = e.key;
  //   // const regex = /[\d]|\.|Backspace/;
  //   const arrValidKeys = ['Backspace'];
  //   if (false) {
  //     // e.preventDefault();
  //   } else {
  //     console.log('You pressed a key: ' + key);
  //   }
  // }

  return (
    <form className={styles.form} {...props} onSubmit={(e) => onSubmit(e)}>
      {Object.keys(stateForm).length !== 0 && (
        <div>
          <p className="mb-2">
            Сумма цен продуктов всех групп: {stateForm.sum}
          </p>
          {stateForm.groups!.length > 0 &&
            stateForm.groups!.map((group: IGroup, index: number) => (
              <div key={`${group.id}-${index}`} className={styles.group}>
                <Button
                  className={clsx(styles.button_delete, 'mb-2')}
                  onClick={async (e) => await deleteGroup(e, group.id)}
                >
                  Delete Group #{group.id}
                </Button>
                <p>
                  Сумма цен продуктов группы № {group.id}: {group.sum}
                </p>
                {group.subGroups &&
                  group.subGroups.length > 0 &&
                  group.subGroups.map((subGroup: ISubGroup) => (
                    <div key={`${subGroup.id}`} className={styles.group}>
                      <Button
                        className={clsx(styles.button_delete, 'mb-2')}
                        onClick={async (e) =>
                          await deleteSubGroup(e, group.id, subGroup.id)
                        }
                      >
                        Delete SubGroup #{subGroup.id} at Group #{group.id}
                      </Button>
                      <p className="mb-2">
                        Сумма цен продуктов подгруппы № {subGroup.id} в группе №{' '}
                        {group.id}: {subGroup.sum}
                      </p>

                      {subGroup.products && subGroup.products.length > 0 && (
                        <table>
                          <thead>
                            <tr>
                              <td colSpan={4}>
                                <p className="text-center">Products</p>
                              </td>
                            </tr>
                          </thead>
                          <tbody>
                            {subGroup.products.map((product) => (
                              <tr key={product.id}>
                                <td className="hidden">
                                  <div className="hidden">
                                    <label htmlFor={`${product.id}-id`}>
                                      ID
                                    </label>
                                    <input
                                      id={`${product.id}-id`}
                                      type="text"
                                      name={`${product.id}-id`}
                                      defaultValue={product.id}
                                      className="hidden"
                                      readOnly
                                    />
                                  </div>
                                </td>
                                <td>
                                  <div className={styles.td}>
                                    <label htmlFor={`${product.id}-name`}>
                                      Название
                                    </label>
                                    <input
                                      id={`${product.id}-name`}
                                      type="text"
                                      name={`${product.id}-name`}
                                      value={product.name}
                                      onChange={async (e) =>
                                        await handleSetName(
                                          group.id,
                                          subGroup.id,
                                          product.id,
                                          e,
                                        )
                                      }
                                      className={clsx({
                                        warning: nameError.includes(product.id),
                                      })}
                                      autoComplete="off"
                                      required
                                    />
                                  </div>
                                </td>
                                <td>
                                  <div className={styles.td}>
                                    <label htmlFor={`${product.id}-price`}>
                                      Цена
                                    </label>
                                    <input
                                      id={`${product.id}-price`}
                                      type="text"
                                      // min="0"
                                      name={`${product.id}-price`}
                                      value={product.price}
                                      onChange={async (e) =>
                                        await handleSetPrice(
                                          group.id,
                                          subGroup.id,
                                          product.id,
                                          e,
                                        )
                                      }
                                      // onKeyDown={(e) => handleOnlyNumber(e)}
                                      className={clsx({
                                        warning: priceError.includes(
                                          product.id,
                                        ),
                                      })}
                                      autoComplete="off"
                                      required
                                    />
                                  </div>
                                </td>
                                <td>
                                  <div className={styles.td}>
                                    <label htmlFor={`${product.id}-count`}>
                                      Количество
                                    </label>
                                    <input
                                      id={`${product.id}-count`}
                                      type="text"
                                      // min="0"
                                      name={`${product.id}-count`}
                                      value={product.count}
                                      onChange={async (e) =>
                                        await handleSetCount(
                                          group.id,
                                          subGroup.id,
                                          product.id,
                                          e,
                                        )
                                      }
                                      className={clsx({
                                        warning: countError.includes(
                                          product.id,
                                        ),
                                      })}
                                      autoComplete="off"
                                      required
                                    />
                                  </div>
                                </td>
                                <td>
                                  <div className={styles.td}>
                                    <label htmlFor={`${product.id}-sum`}>
                                      Сумма
                                    </label>
                                    <input
                                      id={`${product.id}-sum`}
                                      name={`${product.id}-sum`}
                                      type="text"
                                      // min="0"
                                      value={product.sum}
                                      readOnly
                                    />
                                  </div>
                                </td>
                                <td>
                                  <div className={styles.td_button}>
                                    <Button
                                      className={styles.button_delete}
                                      onClick={async (e) =>
                                        await deleteProduct(
                                          e,
                                          group.id,
                                          subGroup.id,
                                          product.id,
                                        )
                                      }
                                    >
                                      Удалить
                                    </Button>
                                  </div>
                                </td>
                              </tr>
                            ))}
                          </tbody>
                        </table>
                      )}
                      <div className={styles.td_button}>
                        <Button
                          className={styles.button_add}
                          onClick={async (e) =>
                            await addProduct(e, group.id, subGroup.id)
                          }
                        >
                          Добавить продукт
                        </Button>
                      </div>
                    </div>
                  ))}
                <Button
                  className={clsx(styles.button_add)}
                  onClick={async (e) => await addSubGroup(e, group.id)}
                >
                  Add SubGroup
                </Button>
              </div>
            ))}
          <Button
            className={clsx(styles.button_add)}
            onClick={async (e) => await addGroup(e)}
          >
            Add Group
          </Button>
        </div>
      )}
      <Button
        type="submit"
        className="mt-3"
        disabled={nameError.length > 0 || priceError.length > 0}
      >
        Submit
      </Button>
    </form>
  );
};

export default Form;
