/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-console */
import { createSlice, current } from '@reduxjs/toolkit';
import { v4 as uuidv4 } from 'uuid';
import { IForm, IGroup } from '@app/interfaces';
import { subGroupFinderById } from '@app/app/lib/utils';

const initialState: IForm = {
  sum: 0,
  groups: [
    {
      id: 1,
      sum: 0,
      subGroups: [
        {
          id: 1,
          groupId: 1,
          sum: 0,
          products: [
            {
              id: 1,
              subGroupId: 1,
              groupId: 1,
              name: 'Product 1',
              sum: 0,
              count: 0,
              price: 0,
            },
          ],
        },
      ],
    },
  ],
};

export const editableFormSlice = createSlice({
  name: 'editableForm',
  initialState,
  reducers: {
    changeEditableForm(state, action) {
      state.sum = action.payload.sum;
      state.groups = action.payload.groups;
    },
    addSubGroupForm(state, action) {
      const group = state.groups.find((gr) => gr.id === action.payload.id);
      const newSubGroups = group && [...group.subGroups];
      newSubGroups &&
        newSubGroups.push({
          id: uuidv4(),
          sum: 0,
          products: [
            {
              id: uuidv4(),
              name: 'Product 1',
              sum: 0,
              count: 0,
              price: 0,
            },
          ],
        });

      const prevGroups: IGroup[] = [...state.groups];

      if (prevGroups && prevGroups.length > 0) {
        prevGroups.map((group) => {
          if (group.id === action.payload.id) {
            if (newSubGroups) {
              group.subGroups = newSubGroups;
            }
            return group;
          }
          return group;
        });
      }
    },
    deleteSubGroupForm(state, action) {
      const { groupId, subGroupId } = action.payload;
      const group = state.groups.find((gr) => gr.id === groupId);

      if (group) {
        const newSubGroups = group.subGroups.filter(
          (sgr) => sgr.id !== subGroupId,
        );

        const prevGroups: IGroup[] = [...state.groups];

        if (prevGroups && prevGroups.length > 0) {
          prevGroups.map((group) => {
            if (group.id === groupId) {
              if (newSubGroups) {
                group.subGroups = newSubGroups;
              }
              return group;
            }
            return group;
          });
        }
      }
    },
    changeProductNameForm(state, action) {
      const { groupId, subGroupId, id, name } = action.payload;
      const subGroup = subGroupFinderById(state, groupId, subGroupId);

      if (subGroup && subGroup.products && subGroup.products.length > 0) {
        subGroup.products.forEach((pr) => {
          if (pr.id === id) {
            pr.name = name;
          }
        });
      }
    },
    changeProductPriceForm(state, action) {
      const { groupId, subGroupId, id, price } = action.payload;
      const subGroup = subGroupFinderById(state, groupId, subGroupId);

      if (subGroup && subGroup.products && subGroup.products.length > 0) {
        subGroup.products.forEach((pr) => {
          if (pr.id === id) {
            pr.price = price;
          }
        });
      }
    },
    changeProductCountForm(state, action) {
      const { groupId, subGroupId, id, count } = action.payload;
      const subGroup = subGroupFinderById(state, groupId, subGroupId);

      if (subGroup && subGroup.products && subGroup.products.length > 0) {
        subGroup.products.forEach((pr) => {
          if (pr.id === id) {
            pr.count = count;
          }
        });
      }
    },
    addProductForm(state, action) {
      const { groupId, subGroupId } = action.payload;
      const subGroup = subGroupFinderById(state, groupId, subGroupId);

      if (subGroup && subGroup.products) {
        subGroup.products.push({
          id: uuidv4(),
          name: `${uuidv4()}`,
          sum: 0,
          count: 0,
          price: 0,
        });
      }
    },
    deleteProductById(state, action) {
      const { groupId, subGroupId, productId } = action.payload;
      const subGroup = subGroupFinderById(state, groupId, subGroupId);

      if (subGroup && subGroup.products && subGroup.products.length > 0) {
        const newProducts = subGroup.products.filter(
          (pr) => pr.id !== productId,
        );

        subGroup.products = newProducts;
      }
    },
    calculateSumForm(state, action) {
      const { groupId, subGroupId, id } = action.payload;
      const subGroup = subGroupFinderById(state, groupId, subGroupId);

      if (action.payload.hasOwnProperty('price')) {
        const { price } = action.payload;

        if (subGroup && subGroup.products && subGroup.products.length > 0) {
          subGroup.products.forEach((pr) => {
            if (pr.id === id) {
              pr.sum = Number((pr.count * price).toFixed(2));
            }
          });
        }
      }

      if (action.payload.hasOwnProperty('count')) {
        const { count } = action.payload;

        if (subGroup && subGroup.products && subGroup.products.length > 0) {
          subGroup.products.forEach((pr) => {
            if (pr.id === id) {
              pr.sum = Number((pr.price * count).toFixed(2));
            }
          });
        }
      }

      if (subGroup && subGroup.products && subGroup.products.length > 0) {
        const sumSubGroups = () => {
          const initialValue = 0;
          const sum = subGroup.products.reduce(
            (acc, currVal) => acc + currVal.sum,
            initialValue,
          );
          return Number(sum.toFixed(2));
        };
        subGroup.sum = sumSubGroups();
      }

      const group = state.groups.find((gr) => gr.id === groupId);

      if (group && group.subGroups && group.subGroups.length > 0) {
        const sumGroups = () => {
          const initialValue = 0;
          const sum = group.subGroups.reduce(
            (acc, currVal) => acc + currVal.sum,
            initialValue,
          );
          return Number(sum.toFixed(2));
        };
        group.sum = sumGroups();
      }

      if (state.groups.length > 0) {
        const initialVallue = 0;
        state.sum = state.groups.reduce(
          (acc, currentValue) => acc + currentValue.sum,
          initialVallue,
        );
      }
    },
    emptyEditableForm(state, _action) {
      state.sum = 0;
      state.groups = [
        {
          id: 1,
          sum: 0,
          subGroups: [
            {
              id: 1,
              groupId: 1,
              sum: 0,
              products: [
                {
                  id: 1,
                  subGroupId: 1,
                  groupId: 1,
                  name: 'Product 1',
                  sum: 0,
                  count: 0,
                  price: 0,
                },
              ],
            },
          ],
        },
      ];
    },
  },
});

export const {
  changeEditableForm,
  emptyEditableForm,
  addSubGroupForm,
  deleteSubGroupForm,
  addProductForm,
  deleteProductById,
  changeProductNameForm,
  changeProductPriceForm,
  changeProductCountForm,
  calculateSumForm,
} = editableFormSlice.actions;

export default editableFormSlice.reducer;
