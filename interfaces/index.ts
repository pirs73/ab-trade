export interface IProduct {
  id: string | number;
  name: string;
  sum: number;
  count: number;
  price: number;
}

export interface StoreProduct extends IProduct {
  subGroupId?: string | number;
  groupId?: string | number;
}
export interface ISubGroup {
  id: string | number;
  sum: number;
  products: IProduct[] | StoreProduct[];
}

export interface StoreSubGroup extends ISubGroup {
  groupId?: string | number;
}
export interface IGroup {
  id: string | number;
  sum: number;
  subGroups: ISubGroup[] | StoreSubGroup[];
}
export interface IForm {
  sum: number;
  groups: IGroup[];
}
